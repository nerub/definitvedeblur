LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)


OPENCVROOT := C:/Users/Nerub/AppData/Local/Android/opencv3-android-sdk-with-contrib-master/OpenCV-android-sdk
#OPENCVROOT := /home/roberto/opencv3-android-sdk-with-contrib-master/OpenCV-android-sdk
#OPENCVROOT := C:/Users/erikb_000/Desktop/Erik/University/EmbeddedSystemsProgramming/Project/opencv3-android-sdk-with-contrib-master/OpenCV-android-sdk

OPENCV_CAMERA_MODULES := on
OPENCV_INSTALL_MODULES := on
OPENCV_LIB_TYPE := SHARED
include $(OPENCVROOT)/sdk/native/jni/OpenCV.mk

LOCAL_LDLIBS := -llog
LOCAL_MODULE := alignment
LOCAL_SRC_FILES := alignment.cpp

include $(BUILD_SHARED_LIBRARY)