package embedded.project.com.opencvnative;

import android.content.Intent;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class Result extends AppCompatActivity {

    //Final picture URI
    private Uri finalPictureUri;

    //Final picture ImageView
    private ImageView finalPictureIw;

    private int finalPicIwID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        setTitle("Final Picture");

        // Get the Uri of the final picture from the Merge Activity
        finalPictureUri = getIntent().getParcelableExtra("finalPictureUri");

        // Show user final image button through a thumbnail.
        finalPicIwID = getResources().getIdentifier("finalPictureView", "id", getPackageName());
        finalPictureIw = (ImageView) findViewById(finalPicIwID);

        //final picture thumbnail
        Picasso.get().load(finalPictureUri).resize(500,500).centerCrop().into(finalPictureIw);

        //open gallery when touch the thumbnail
        finalPictureIw.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent tIntent = new Intent();
                tIntent.setAction(android.content.Intent.ACTION_VIEW);
                tIntent.setDataAndType(finalPictureUri, "image/*");
                startActivity(tIntent);
            }
        });

        //Restart from MainActivity
        Button tryAgainButton = (Button)findViewById(R.id.tryAgainButton);

        tryAgainButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), MainActivity.class);
                startActivityForResult(myIntent, 0);
            }

        });
    }

}
