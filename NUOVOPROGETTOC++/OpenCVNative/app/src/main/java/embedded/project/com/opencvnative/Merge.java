package embedded.project.com.opencvnative;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class Merge extends AppCompatActivity {

    //Total number of pictures to be selected
    private static int TOTAL_PICS;

    //List of selected pictures URIs
    private ArrayList<Uri> uriList = new ArrayList<>();

    //List of ImageViews
    private ArrayList<ImageView> iwList = new ArrayList<>();

    //List of ImageViews names
    private ArrayList<String> iwNameList = new ArrayList<>();

    //List of ImageViews IDs
    private ArrayList<Integer> iwIdList = new ArrayList<>();

    //Final picture URI
    private Uri finalPictureUri;

    //Test URIs
    private ArrayList<Uri> uriTestList = new ArrayList<>();

    //Alignment condition
    private boolean alignmentActivated;

    //Speed condition
    private boolean fast;

    //Initialize a Toast to null
    private Toast mToast = null;

    //load native library
    static {
        System.loadLibrary("alignment");
    }

    /**
     * Native methods implemented by the 'alignment' native library,
     * which is packaged with this application.
     */
    public native void alignImagesJNI(long addrIm1, long addrIm2, long addrIm1Reg, long addrH);
    public native void wfbaJNI(long addrIm1, long addrIm2, long addrIm3, long addrIm4, long addrIm5, long addrIm6, long addrRes, boolean alignOnOff);
    public native void wfbaFastJNI(long addrIm1, long addrIm2, long addrIm3, long addrRes, boolean alignOnOff);

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.splash_screen);

        //initialize switch variables
        TOTAL_PICS = 6;
        alignmentActivated = false;
        fast = false;

        //get uri from main activity
        uriList = getIntent().getParcelableArrayListExtra("uriList");

        //get alignment condition from main activity
        alignmentActivated  = getIntent().getExtras().getBoolean("condition");

        //get speed condition from main activity
        fast = getIntent().getExtras().getBoolean("speed");

        //set the total pics according to the selected speed
        if(fast) {
            TOTAL_PICS = 3;
        }

        // set up and start the initialization thread
        final Handler handler = new Handler();
        new Thread() {
            public void run() {
                // Do time-consuming initialization.
                try {
                    uriTestList = wfba(uriList,alignmentActivated);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                // When done:
                handler.post(new Runnable() {
                    public void run() {
                        // set up the real UI
                        setContentView(R.layout.activity_merge);
                        setTitle("Selected Pictures");

                        //fill all the lists
                        for (int i = 0; i < TOTAL_PICS; i++) {

                            //set imgView names
                            iwNameList.add("imgView" + i);

                            //get imgView IDs
                            iwIdList.add(getResources().getIdentifier(iwNameList.get(i), "id", getPackageName()));

                            //add imgViews to a list
                            iwList.add((ImageView) findViewById(iwIdList.get(i)));

                            //create thumbnails
                            Picasso.get().load(uriList.get(i)).resize(500,500).centerCrop().into(iwList.get(i));

                            //index to make onClick method work
                            final int j=i;

                            //open gallery when touch a thumbnail
                            iwList.get(j).setOnClickListener(new View.OnClickListener() {
                                public void onClick(View view) {
                                    Intent tIntent = new Intent();
                                    tIntent.setAction(android.content.Intent.ACTION_VIEW);
                                    tIntent.setDataAndType(uriList.get(j), "image/*");
                                    startActivity(tIntent);
                                }
                            });
                        }

                        // Show user final image button.
                        Button mergePicturesButton = (Button)findViewById(R.id.mergeButton);

                        mergePicturesButton.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View view) {
                                Intent myIntent = new Intent(view.getContext(), Result.class);
                                myIntent.putExtra("finalPictureUri", finalPictureUri);
                                startActivityForResult(myIntent, 0);
                            }

                        });

                        //align button
                        Button alignButton = (Button)findViewById(R.id.testButton);

                        alignButton.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View view) {

                                //if alignment was selected in the Main Activity, you will see the aligned pictures
                                if (alignmentActivated){
                                    Intent myIntent = new Intent(view.getContext(), Test.class);
                                    myIntent.putExtra("uriTestList", uriTestList);
                                    myIntent.putExtra("finalPictureUri", finalPictureUri);
                                    myIntent.putExtra("speed", fast);
                                    startActivityForResult(myIntent, 0);
                                }

                                //if alignment was not selected in the Main Activity, pressing alignButton you get a toast message
                                else {
                                    //avoid toast accumulation
                                    if (mToast != null) mToast.cancel();
                                    mToast = Toast.makeText(getApplicationContext(), "Alignment not selected previously", Toast.LENGTH_LONG);
                                    mToast.show();
                                }
                            }
                        });
                    }
                });
            }
        }.start();








    }

    //Weighted Fourier Burst Accumulation Algorithm
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public ArrayList<Uri> wfba(ArrayList<Uri> uriImgList,boolean alignOnOff) throws IOException {

        final int TOTPICS = uriImgList.size();
        ArrayList<Mat> matList = new ArrayList<>();
        ArrayList<Mat> alignedMatList = new ArrayList<>();
        ArrayList<Bitmap> alignedBitmaps = new ArrayList<>();
        ArrayList<Uri> alignedBitmapUris = new ArrayList<>();
        Bitmap bmt;

        Mat homo = new Mat( 3, 3, CvType.CV_64FC1 );//used only if alignment is on
        Mat res = new Mat( 3, 4, CvType.CV_64FC1 );

        //create a Mat for each selected picture
        for (int i = 0; i < TOTPICS; i++) {
            String realPath = getUriRealPath(getApplicationContext(), uriImgList.get(i));
            matList.add(Imgcodecs.imread(realPath));
        }

        //Condition to use or not use alignment
        if(alignOnOff){//align ON
            //Align images through native method

            for (int i = 0; i < TOTPICS; i++) {
                Mat imReg = new Mat( 3, 4, CvType.CV_64FC1 );//a ogni iterazione creo nuova matrice
                //call native method
                if(i > 0)
                    alignImagesJNI(matList.get(0).getNativeObjAddr(), matList.get(i).getNativeObjAddr(), imReg.getNativeObjAddr(), homo.getNativeObjAddr());
                else
                    imReg=matList.get(0);
                alignedMatList.add(imReg);

                //bitmap of the i-th image
                Bitmap bp = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uriImgList.get(i));

                //create aligned bitmap from the aligned Mat
                Bitmap alignedBMP = Bitmap.createBitmap(bp.getWidth(), bp.getHeight(), Bitmap.Config.RGB_565);

                //channels inversion
                Imgproc.cvtColor(imReg,imReg,Imgproc.COLOR_BGR2RGB);
                Utils.matToBitmap(imReg, alignedBMP);
                alignedBitmaps.add(alignedBMP);
            }

            Log.d("Timing","Foto tutte allineate");

            //add aligned images to the uris list
            for (int i = 0; i < TOTPICS; i++){
                alignedBitmapUris.add(getImageUri(getApplicationContext(),alignedBitmaps.get(i)));
            }
            //end of alignment

        }else{
                alignedMatList=matList;
        }//end of alignment condition evaluation

        if (fast) {
            //apply fast wfba native algorithm
            wfbaFastJNI(alignedMatList.get(0).getNativeObjAddr(), alignedMatList.get(1).getNativeObjAddr(),
                    alignedMatList.get(2).getNativeObjAddr(), res.getNativeObjAddr(), alignmentActivated);
        }

        else {
            //apply wfba native algorithm
            wfbaJNI(alignedMatList.get(0).getNativeObjAddr(), alignedMatList.get(1).getNativeObjAddr(),
                    alignedMatList.get(2).getNativeObjAddr(), alignedMatList.get(3).getNativeObjAddr(),
                    alignedMatList.get(4).getNativeObjAddr(), alignedMatList.get(5).getNativeObjAddr(),
                    res.getNativeObjAddr(), alignmentActivated);
        }

        Bitmap bpt = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uriImgList.get(0));
        Bitmap finalPic = Bitmap.createBitmap(bpt.getWidth(),bpt.getHeight() , Bitmap.Config.RGB_565);
        Utils.matToBitmap(res, finalPic);
        finalPictureUri = getImageUri(getApplicationContext(),finalPic);

        //return the URIs related to the aligned bitmaps
        return alignedBitmapUris;
    }

    //Get Uri from Bitmap
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    //Get real path from Uri
    /* Get uri related content real local file path. */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private String getUriRealPath(Context ctx, Uri uri)
    {
        String ret = "";

        if( isAboveKitKat() )
        {
            // Android OS above sdk version 19.
            ret = getUriRealPathAboveKitkat(ctx, uri);
        }else
        {
            // Android OS below sdk version 19
            ret = getImageRealPath(getContentResolver(), uri, null);
        }

        return ret;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private String getUriRealPathAboveKitkat(Context ctx, Uri uri)
    {
        String ret = "";

        if(ctx != null && uri != null) {

            if(isContentUri(uri))
            {
                if(isGooglePhotoDoc(uri.getAuthority()))
                {
                    ret = uri.getLastPathSegment();
                }else {
                    ret = getRealPathFromURI(getApplicationContext(),uri);
                }
            }else if(isFileUri(uri)) {
                ret = uri.getPath();
            }else if(isDocumentUri(ctx, uri)){

                // Get uri related document id.
                String documentId = DocumentsContract.getDocumentId(uri);

                // Get uri authority.
                String uriAuthority = uri.getAuthority();

                if(isMediaDoc(uriAuthority))
                {
                    String idArr[] = documentId.split(":");
                    if(idArr.length == 2)
                    {
                        // First item is document type.
                        String docType = idArr[0];

                        // Second item is document real id.
                        String realDocId = idArr[1];

                        // Get content uri by document type.
                        Uri mediaContentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                        if("image".equals(docType))
                        {
                            mediaContentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                        }else if("video".equals(docType))
                        {
                            mediaContentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                        }else if("audio".equals(docType))
                        {
                            mediaContentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                        }

                        // Get where clause with real document id.
                        String whereClause = MediaStore.Images.Media._ID + " = " + realDocId;

                        ret = getImageRealPath(getContentResolver(), mediaContentUri, whereClause);
                    }

                }else if(isDownloadDoc(uriAuthority))
                {
                    // Build download uri.
                    Uri downloadUri = Uri.parse("content://downloads/public_downloads");

                    // Append download document id at uri end.
                    Uri downloadUriAppendId = ContentUris.withAppendedId(downloadUri, Long.valueOf(documentId));

                    ret = getRealPathFromURI(getApplicationContext(), downloadUriAppendId);

                }else if(isExternalStoreDoc(uriAuthority))
                {
                    String idArr[] = documentId.split(":");
                    if(idArr.length == 2)
                    {
                        String type = idArr[0];
                        String realDocId = idArr[1];

                        if("primary".equalsIgnoreCase(type))
                        {
                            ret = Environment.getExternalStorageDirectory() + "/" + realDocId;
                        }
                    }
                }
            }
        }

        return ret;
    }

    /* Check whether current android os version is bigger than kitkat or not. */
    private boolean isAboveKitKat()
    {
        boolean ret = false;
        ret = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        return ret;
    }

    /* Check whether this uri represent a document or not. */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private boolean isDocumentUri(Context ctx, Uri uri)
    {
        boolean ret = false;
        if(ctx != null && uri != null) {
            ret = DocumentsContract.isDocumentUri(ctx, uri);
        }
        return ret;
    }

    /* Check whether this uri is a content uri or not.
    *  content uri like content://media/external/images/media/1302716
    *  */
    private boolean isContentUri(Uri uri)
    {
        boolean ret = false;
        if(uri != null) {
            String uriSchema = uri.getScheme();
            if("content".equalsIgnoreCase(uriSchema))
            {
                ret = true;
            }
        }
        return ret;
    }

    /* Check whether this uri is a file uri or not.
    *  file uri like file:///storage/41B7-12F1/DCIM/Camera/IMG_20180211_095139.jpg
    * */
    private boolean isFileUri(Uri uri)
    {
        boolean ret = false;
        if(uri != null) {
            String uriSchema = uri.getScheme();
            if("file".equalsIgnoreCase(uriSchema))
            {
                ret = true;
            }
        }
        return ret;
    }

    /* Check whether this document is provided by ExternalStorageProvider. */
    private boolean isExternalStoreDoc(String uriAuthority)
    {
        boolean ret = false;

        if("com.android.externalstorage.documents".equals(uriAuthority))
        {
            ret = true;
        }

        return ret;
    }

    /* Check whether this document is provided by DownloadsProvider. */
    private boolean isDownloadDoc(String uriAuthority)
    {
        boolean ret = false;

        if("com.android.providers.downloads.documents".equals(uriAuthority))
        {
            ret = true;
        }

        return ret;
    }

    /* Check whether this document is provided by MediaProvider. */
    private boolean isMediaDoc(String uriAuthority)
    {
        boolean ret = false;

        if("com.android.providers.media.documents".equals(uriAuthority))
        {
            ret = true;
        }

        return ret;
    }

    /* Check whether this document is provided by google photos. */
    private boolean isGooglePhotoDoc(String uriAuthority)
    {
        boolean ret = false;

        if("com.google.android.apps.photos.content".equals(uriAuthority))
        {
            ret = true;
        }

        return ret;
    }

    /* Return uri represented document file real local path.*/
    private String getImageRealPath(ContentResolver contentResolver, Uri uri, String whereClause)
    {
        String ret = "";

        // Query the uri with condition.
        Cursor cursor = contentResolver.query(uri, null, whereClause, null, null);

        if(cursor!=null)
        {
            boolean moveToFirst = cursor.moveToFirst();
            if(moveToFirst)
            {

                // Get columns name by uri type.
                String columnName = MediaStore.Images.Media.DATA;

                if( uri== MediaStore.Images.Media.EXTERNAL_CONTENT_URI )
                {
                    columnName = MediaStore.Images.Media.DATA;
                }else if( uri== MediaStore.Audio.Media.EXTERNAL_CONTENT_URI )
                {
                    columnName = MediaStore.Audio.Media.DATA;
                }else if( uri== MediaStore.Video.Media.EXTERNAL_CONTENT_URI )
                {
                    columnName = MediaStore.Video.Media.DATA;
                }

                // Get column index.
                int imageColumnIndex = cursor.getColumnIndex(columnName);

                // Get column value which is the uri related file local path.
                ret = cursor.getString(imageColumnIndex);
            }
        }
        return ret;
    }

    //test for Xiaomi smartphone
    public static String getRealPathFromURI(Context context, Uri uri) {
        String filePath = "";
        String wholeID = DocumentsContract.getDocumentId(uri);

        // Split at colon, use second item in the array
        String id = wholeID.split(":")[1];

        String[] column = {MediaStore.Images.Media.DATA};

        // where id is equal to
        String sel = MediaStore.Images.Media._ID + "=?";

        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                column, sel, new String[]{id}, null);

        int columnIndex = cursor.getColumnIndex(column[0]);

        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;

/*
        String result;
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = uri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
 */
    }
}



